package main.chebegroup.study.pointmaster;

import java.util.ArrayList;

import main.chebegroup.study.utils.console.ConsoleLog;
import main.chebegroup.study.utils.csv.CSVTable;
import main.chebegroup.study.utils.csv.CSVUtils;
import main.chebegroup.study.utils.geometry.Broken2D;
import main.chebegroup.study.utils.geometry.FlatSegment2D;
import main.chebegroup.study.utils.geometry.MathUtils;
import main.chebegroup.study.utils.geometry.Point1D;
import main.chebegroup.study.utils.geometry.Point2D;
import main.chebegroup.study.utils.geometry.Segment1D;
import main.chebegroup.study.utils.geometry.Segment2D;
import main.chebegroup.study.utils.geometry.csv.CSVGeometryUtils;
import main.chebegroup.study.utils.geometry.geodesy.Axes2D;

public class PointMaster 
{
	
	
	
	public static void main(String[] args) 
	{
        //ConsoleLog.error(PointMaster.class, "babushka");
		
		Point1D p1 = new Point1D(10);
		Point1D p2 = new Point1D(-8);
		
		double d = p1.getDist(p2);
		System.out.println(d);
		
		Segment1D s1 = new Segment1D(p1, p2);
		boolean b1 = s1.isPointInSegment(new Point1D(-100));
		System.out.println(b1);
		
		Point2D po0 = new Point2D(0, 0);
		
		Point2D po1 = new Point2D(-100, -100);
		Point2D po2 = new Point2D(100, 100);
		
		Point2D po3 = new Point2D(-100, -99);
		Point2D po4 = new Point2D(-10, 0);
		
		Segment2D seg1 = new Segment2D(po1, po2);
		Segment2D seg2 = new Segment2D(po3, po4);
		
		System.out.println(seg1.isSegmentsCross(seg2));
		System.out.println(seg1.isPointInSegment(po0));
		System.out.println(seg2.isPointInSegment(po0));
		
		ConsoleLog.separator();
		
		FlatSegment2D fseg1 = new FlatSegment2D(new Point2D (4, 0), new Point2D(4, 8));
		fseg1.rotateSegment(new Point2D(0, 0), -90, 4);
		ArrayList<Point2D> l = fseg1.getPoints();
		
		System.out.println(MathUtils.round(l.get(0).getX(), 4) );
		System.out.println(l.get(0).getY());
		
		System.out.println(l.get(1).getX());
		System.out.println(l.get(1).getY());
		
		ConsoleLog.separator();
		
		System.out.println(Axes2D.AXIS_X);
		
		ConsoleLog.separator();
		
		CSVTable ta = CSVUtils.openCSVFileAsTable("point.csv");
		Point2D pointp = CSVGeometryUtils.getPoint2DFromCSVTable(ta);
		MathUtils.logObject(pointp);
		
		CSVTable ta1 = CSVUtils.openCSVFileAsTable("points.csv");
		Broken2D broken = CSVGeometryUtils.getBroken2DFromCSVTable(ta1);
		MathUtils.logObject(broken);
		
		//CSVUtils.writeCSVTable("mama.csv", ta);
		
		ConsoleLog.separator();
		
		Broken2D broken2 = new Broken2D(new Point2D(0, 4), new Point2D(4, 4), new Point2D(8, 4));
		CSVTable ta2 = CSVGeometryUtils.getCSVTableFromBroken2D(broken2);
		CSVUtils.writeCSVTable("CHECK.csv", ta2);
		
    }
}
