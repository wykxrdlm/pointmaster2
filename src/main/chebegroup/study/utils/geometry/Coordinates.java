package main.chebegroup.study.utils.geometry;

import java.util.ArrayList;

import main.chebegroup.study.utils.console.ConsoleLog;

public class Coordinates 
{
	private ArrayList<Double> coordinates;
	
	public Coordinates(double x)
	{
		this.coordinates = new ArrayList<Double>();
		
		this.coordinates.add(x);
	}
	
	public Coordinates(double x, double y)
	{
		this.coordinates = new ArrayList<Double>();
		
		this.coordinates.add(x);
		this.coordinates.add(y);
	}
	
	public Coordinates(double x, double y, double z)
	{
		this.coordinates = new ArrayList<Double>();
		
		this.coordinates.add(x);
		this.coordinates.add(y);
		this.coordinates.add(z);
	}
	
	public Coordinates(ArrayList<Double> coordsList)
	{
		this.coordinates = coordsList;
	}
	
	public void setCoordinate(int dimension, double coordinate)
	{
		int maxDimension = this.coordinates.size();
		
		if(dimension < maxDimension)
		{
			this.coordinates.set(dimension, coordinate);
		}
		else
		{
			ConsoleLog.error(this.getClass(), "Incorrect number of dimensions in setCoordinate()");
		}
	}
	
	public double getCoordinate(int dimension)
	{
		int maxDimension = this.coordinates.size();
		
		if(dimension < maxDimension)
		{
			return this.coordinates.get(dimension);
		}
		else
		{
			ConsoleLog.error(this.getClass(), "Incorrect number of dimensions in getCoordinate()");
			return 0;
		}
	}
	
	public int getDimensionsNumber()
	{
		return this.coordinates.size();
	}
	
	public boolean is1D()
	{
		if (this.coordinates.size() == 1)
			return true;
		else
			return false;
	}
	
	public boolean is2D()
	{
		if (this.coordinates.size() == 2)
			return true;
		else
			return false;
	}
	
	public boolean is3D()
	{
		if (this.coordinates.size() == 3)
			return true;
		else
			return false;
	}
}
