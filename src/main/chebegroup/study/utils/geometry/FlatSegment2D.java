package main.chebegroup.study.utils.geometry;

public class FlatSegment2D extends Segment2D
{
	public FlatSegment2D(Point2D p1, Point2D p2)
	{
		super(p1, p2);
	}
	
	public void rotateSegment(Point2D point, double rotateAngle)
	{
		rotateAngle = MathUtils.angleToPI(rotateAngle);
		
		double xfp = Math.cos(rotateAngle) * (this.first_point.getX() - point.getX()) - Math.sin(rotateAngle) * (this.first_point.getY() - point.getY()) + point.getX();
		double yfp = Math.cos(rotateAngle) * (this.first_point.getY() - point.getY()) + Math.sin(rotateAngle) * (this.first_point.getX() - point.getX()) + point.getY(); 
		
		double xsp = Math.cos(rotateAngle) * (this.second_point.getX() - point.getX()) - Math.sin(rotateAngle) * (this.second_point.getY() - point.getY()) + point.getX();
		double ysp = Math.cos(rotateAngle) * (this.second_point.getY() - point.getY()) + Math.sin(rotateAngle) * (this.second_point.getX() - point.getX()) + point.getY();
	
		this.first_point = new Point2D(xfp, yfp);
		this.second_point = new Point2D(xsp, ysp);
	}
	
	public void rotateSegment(Point2D point, double rotateAngle, int rounded)
	{
		rotateAngle = MathUtils.angleToPI(rotateAngle);
		
		double xfp = Math.cos(rotateAngle) * (this.first_point.getX() - point.getX()) - Math.sin(rotateAngle) * (this.first_point.getY() - point.getY()) + point.getX();
		double yfp = Math.cos(rotateAngle) * (this.first_point.getY() - point.getY()) + Math.sin(rotateAngle) * (this.first_point.getX() - point.getX()) + point.getY(); 
		
		double xsp = Math.cos(rotateAngle) * (this.second_point.getX() - point.getX()) - Math.sin(rotateAngle) * (this.second_point.getY() - point.getY()) + point.getX();
		double ysp = Math.cos(rotateAngle) * (this.second_point.getY() - point.getY()) + Math.sin(rotateAngle) * (this.second_point.getX() - point.getX()) + point.getY(); 
	
		this.first_point = new Point2D(MathUtils.round(xfp, rounded), MathUtils.round(yfp, rounded));
		this.second_point = new Point2D(MathUtils.round(xsp, rounded), MathUtils.round(ysp, rounded));
	}
	
	public void rotateSegmentWithPi(Point2D point, double piAngle)
	{
		double rotateAngle = piAngle;
		
		double xfp = Math.cos(rotateAngle) * (this.first_point.getX() - point.getX()) - Math.sin(rotateAngle) * (this.first_point.getY() - point.getY()) + point.getX();
		double yfp = Math.cos(rotateAngle) * (this.first_point.getY() - point.getY()) + Math.sin(rotateAngle) * (this.first_point.getX() - point.getX()) + point.getY(); 
		
		double xsp = -1 * Math.sin(rotateAngle) * (this.second_point.getY() - point.getY()) + Math.cos(rotateAngle) * (this.second_point.getX() - point.getX()) + point.getX();
		double ysp = Math.cos(rotateAngle) * (this.second_point.getY() - point.getY()) + Math.sin(rotateAngle) * (this.second_point.getX() - point.getX()) + point.getY(); 
	
		this.first_point = new Point2D(xfp, yfp);
		this.second_point = new Point2D(xsp, ysp);
	}
	
	public void rotateSegmentWithPi(Point2D point, double piAngle, int rounded)
	{
		double rotateAngle = piAngle;
		
		double xfp = Math.cos(rotateAngle) * (this.first_point.getX() - point.getX()) - Math.sin(rotateAngle) * (this.first_point.getY() - point.getY()) + point.getX();
		double yfp = Math.cos(rotateAngle) * (this.first_point.getY() - point.getY()) + Math.sin(rotateAngle) * (this.first_point.getX() - point.getX()) + point.getY(); 
		
		double xsp = Math.cos(rotateAngle) * (this.second_point.getX() - point.getX()) - Math.sin(rotateAngle) * (this.second_point.getY() - point.getY()) + point.getX();
		double ysp = Math.cos(rotateAngle) * (this.second_point.getY() - point.getY()) + Math.sin(rotateAngle) * (this.second_point.getX() - point.getX()) + point.getY();
	
		this.first_point = new Point2D(MathUtils.round(xfp, rounded), MathUtils.round(yfp, rounded));
		this.second_point = new Point2D(MathUtils.round(xsp, rounded), MathUtils.round(ysp, rounded));
	}
	
	public void rotateSegmentFromStart(double rotateAngle)
	{
		rotateAngle = MathUtils.angleToPI(rotateAngle);
		
		double xfp = Math.cos(rotateAngle) * this.first_point.getX() - Math.sin(rotateAngle) * this.first_point.getY();
		double yfp = Math.cos(rotateAngle) * this.first_point.getY() + Math.sin(rotateAngle) * this.first_point.getX(); 
		
		double xsp = Math.cos(rotateAngle) * this.second_point.getX() - Math.sin(rotateAngle) * this.second_point.getY();
		double ysp = Math.cos(rotateAngle) * this.second_point.getY() + Math.sin(rotateAngle) * this.second_point.getX(); 
	
		this.first_point = new Point2D(xfp, yfp);
		this.second_point = new Point2D(xsp, ysp);
	}
	
	public void rotateSegmentFromStart(double rotateAngle, int rounded)
	{
		rotateAngle = MathUtils.angleToPI(rotateAngle);
		
		double xfp = Math.cos(rotateAngle) * this.first_point.getX() - Math.sin(rotateAngle) * this.first_point.getY();
		double yfp = Math.cos(rotateAngle) * this.first_point.getY() + Math.sin(rotateAngle) * this.first_point.getX(); 
		
		double xsp = Math.cos(rotateAngle) * this.second_point.getX() - Math.sin(rotateAngle) * this.second_point.getY();
		double ysp = Math.cos(rotateAngle) * this.second_point.getY() + Math.sin(rotateAngle) * this.second_point.getX(); 
	
		this.first_point = new Point2D(MathUtils.round(xfp, rounded), MathUtils.round(yfp, rounded));
		this.second_point = new Point2D(MathUtils.round(xsp, rounded), MathUtils.round(ysp, rounded));
	}
	
	public void rotateSegmentFromStartWithPi(double rotateAngle)
	{
		double xfp = Math.cos(rotateAngle) * this.first_point.getX() - Math.sin(rotateAngle) * this.first_point.getY();
		double yfp = Math.cos(rotateAngle) * this.first_point.getY() + Math.sin(rotateAngle) * this.first_point.getX(); 
		
		double xsp = Math.cos(rotateAngle) * this.second_point.getX() - Math.sin(rotateAngle) * this.second_point.getY();
		double ysp = Math.cos(rotateAngle) * this.second_point.getY() + Math.sin(rotateAngle) * this.second_point.getX(); 
	
		this.first_point = new Point2D(xfp, yfp);
		this.second_point = new Point2D(xsp, ysp);
	}
	
	public void rotateSegmentFromStartWithPi(double rotateAngle, int rounded)
	{
		double xfp = Math.cos(rotateAngle) * this.first_point.getX() - Math.sin(rotateAngle) * this.first_point.getY();
		double yfp = Math.cos(rotateAngle) * this.first_point.getY() + Math.sin(rotateAngle) * this.first_point.getX(); 
		
		double xsp = Math.cos(rotateAngle) * this.second_point.getX() - Math.sin(rotateAngle) * this.second_point.getY();
		double ysp = Math.cos(rotateAngle) * this.second_point.getY() + Math.sin(rotateAngle) * this.second_point.getX(); 
	
		this.first_point = new Point2D(MathUtils.round(xfp, rounded), MathUtils.round(yfp, rounded));
		this.second_point = new Point2D(MathUtils.round(xsp, rounded), MathUtils.round(ysp, rounded));
	}
	
	
}
