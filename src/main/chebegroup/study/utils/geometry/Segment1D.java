package main.chebegroup.study.utils.geometry;

import java.util.ArrayList;

import main.chebegroup.study.utils.console.ConsoleLog;

public class Segment1D 
{
	Point1D first_point;
	Point1D second_point;
	
	public Segment1D(Point1D p1, Point1D p2)
	{
		if(p1.getCoordinates().getCoordinate(0) != p2.getCoordinates().getCoordinate(0))
		{
			this.first_point = p1;
			this.second_point = p2;
		}
		else
		{
			ConsoleLog.error(this.getClass(), "Incorrect points in the constructor of this class");
		}
	}
	
	public Point1D getFirstPoint()
	{
		return this.first_point;
	}
	
	public Point1D getSecondPoint()
	{
		return this.second_point;
	}
	
	public Point1D getMinPoint()
	{
		Point1D min_point;
		//Point1D max_point;
		
		if (this.first_point.getCoordinates().getCoordinate(0) > this.second_point.getCoordinates().getCoordinate(0))
		{
			//max_point = this.first_point;
			min_point = this.second_point;
			
			return min_point;
		}
		
		if (this.second_point.getCoordinates().getCoordinate(0) > this.first_point.getCoordinates().getCoordinate(0))
		{
			//max_point = this.second_point;
			min_point = this.first_point;
			
			return min_point;
		}
		else
		{
			ConsoleLog.error(this.getClass(), "Points in segment are the same");
			return new Point1D(0);
		}
	}
	
	public Point1D getMaxPoint()
	{
		//Point1D min_point;
		Point1D max_point;
		
		if (this.first_point.getCoordinates().getCoordinate(0) > this.second_point.getCoordinates().getCoordinate(0))
		{
			max_point = this.first_point;
			//min_point = this.second_point;
			
			return max_point;
		}
		
		if (this.second_point.getCoordinates().getCoordinate(0) > this.first_point.getCoordinates().getCoordinate(0))
		{
			max_point = this.second_point;
			//min_point = this.first_point;
			
			return max_point;
		}
		else
		{
			ConsoleLog.error(this.getClass(), "Points in segment are the same");
			return new Point1D(0);
		}
	}
	
	public ArrayList<Point1D> getPoints()
	{
		ArrayList<Point1D> list = new ArrayList<Point1D>();
		Point1D min_point;
		Point1D max_point;
		
		if (this.first_point.getCoordinates().getCoordinate(0) > this.second_point.getCoordinates().getCoordinate(0))
		{
			max_point = this.first_point;
			min_point = this.second_point;
			
			list.add(min_point);
			list.add(max_point);
			
			return list;
		}
		
		if (this.second_point.getCoordinates().getCoordinate(0) > this.first_point.getCoordinates().getCoordinate(0))
		{
			max_point = this.second_point;
			min_point = this.first_point;
			
			list.add(min_point);
			list.add(max_point);
			
			return list;
		}
		else
		{
			ConsoleLog.error(this.getClass(), "Points in segment are the same");
			return list;
		}
	}
	
	public double length()
	{
		return first_point.getDist(second_point);
	}
	
	public boolean isPointInSegment(Point1D point)
	{
		double minCoord = Math.min(this.first_point.getCoordinates().getCoordinate(0), this.second_point.getCoordinates().getCoordinate(0));
		double maxCoord = Math.max(this.first_point.getCoordinates().getCoordinate(0), this.second_point.getCoordinates().getCoordinate(0));
		double pointCoord = point.getCoordinates().getCoordinate(0);
		
		return (minCoord <= pointCoord && maxCoord >= pointCoord);
	}
	
	public boolean isSegmentInSegment(Segment1D segment)
	{
		double minCoord = Math.min(this.first_point.getCoordinates().getCoordinate(0), this.second_point.getCoordinates().getCoordinate(0));
		double maxCoord = Math.max(this.first_point.getCoordinates().getCoordinate(0), this.second_point.getCoordinates().getCoordinate(0));
		
		double minSegmentCoord = segment.getMinPoint().getCoordinates().getCoordinate(0);
		double maxSegmentCoord = segment.getMaxPoint().getCoordinates().getCoordinate(0);
		
		boolean flag = (minSegmentCoord <= minCoord && maxSegmentCoord >= minCoord) || (maxSegmentCoord >= maxCoord && minSegmentCoord <= maxCoord);
		boolean flag1 = (minSegmentCoord <= minCoord && maxSegmentCoord >= maxCoord) || (maxSegmentCoord <= maxCoord && minSegmentCoord >= minCoord);
		
		return (flag || flag1);
	}
}
