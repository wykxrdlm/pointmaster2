package main.chebegroup.study.utils.geometry;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MathUtils 
{
	public static double round(double value, int places) 
	{
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = BigDecimal.valueOf(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public static double angleToPI(double angle)
	{
		return (angle % 360 / 180) * Math.PI;
	}
	
	public static void logObject(Point1D obj)
	{
		System.out.println("Point1D:" + "x=" + String.valueOf(obj.getX()));
	}
	
	public static void logObject(Point2D obj)
	{
		System.out.println("Point2D:" + "x=" + String.valueOf(obj.getX()) + "y=" + String.valueOf(obj.getY()));
	}
	
	public static void logObject(Segment1D obj)
	{
		System.out.println("Segment1D:" + "start_point_x=" + String.valueOf(obj.getFirstPoint().getX()) + "end_point_x=" + String.valueOf(obj.getSecondPoint().getX()));
	}
	
	public static void logObject(Segment2D obj)
	{
		System.out.println("Segment2D:" + "start_point_x=" + String.valueOf(obj.getFirstPoint().getX()) + "start_point_y=" + String.valueOf(obj.getFirstPoint().getY()) + "end_point_x=" + String.valueOf(obj.getSecondPoint().getX())  + "end_point_y=" + String.valueOf(obj.getSecondPoint().getY()));
	}
	
	public static void logObject(FlatSegment2D obj)
	{
		System.out.println("FlatSegment2D:" + "start_point_x=" + String.valueOf(obj.getFirstPoint().getX()) + "start_point_y=" + String.valueOf(obj.getFirstPoint().getY()) + "end_point_x=" + String.valueOf(obj.getSecondPoint().getX())  + "end_point_y=" + String.valueOf(obj.getSecondPoint().getY()));
	}
	
	public static void logObject(Broken2D obj)
	{
		System.out.print("Broken2D:");
		System.out.print("Points:");
		for(int i = 0; i < obj.getAllPoints().size(); i++)
		{
			Point2D obj1 = obj.getAllPoints().get(i);
			System.out.print("Point2D:" + "x=" + String.valueOf(obj1.getX()) + "y=" + String.valueOf(obj1.getY()));
		}
		
		System.out.print("Segments:");
		for(int i = 0; i < obj.getAllSegments().size(); i++)
		{
			Segment2D obj1 = obj.getAllSegments().get(i);
			System.out.print("Segment2D:" + "start_point_x=" + String.valueOf(obj1.getFirstPoint().getX()) + "start_point_y=" + String.valueOf(obj1.getFirstPoint().getY()) + "end_point_x=" + String.valueOf(obj1.getSecondPoint().getX())  + "end_point_y=" + String.valueOf(obj1.getSecondPoint().getY()));
		}
		
		System.out.print("\n");
	}
}
