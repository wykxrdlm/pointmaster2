package main.chebegroup.study.utils.geometry.geodesy;

import java.util.HashMap;

import main.chebegroup.study.utils.console.ConsoleLog;
import main.chebegroup.study.utils.geometry.Coordinates;
import main.chebegroup.study.utils.geometry.MathUtils;
import main.chebegroup.study.utils.geometry.Point2D;

public class GeodesyPoint2D extends Point2D
{
	private HashMap<String, String> attributes;
	
	public GeodesyPoint2D(double coordX, double coordY, HashMap<String, String> map)
	{
		super(coordX, coordY);
		this.attributes = map;
	}
	
	public GeodesyPoint2D(double coordX, double coordY)
	{
		super(coordX, coordY);
	}
	
	public GeodesyPoint2D(Coordinates coordinates, HashMap<String, String> map) 
	{
		super(coordinates);
		this.attributes = map;
	}

	public GeodesyPoint2D(Coordinates coordinates) 
	{
		super(coordinates);
	}
	
	public void newAttribute(String key, String value)
	{
		if(!attributes.containsKey(key))
		{
			attributes.put(key, value);
		}
	}
	
	public void newAttributeForce(String key, String value)
	{
		attributes.remove(key);
		attributes.put(key, value);
	}
	
	public boolean isAttributeExist(String key)
	{
		return (attributes.containsKey(key));
	}
	
	public boolean isValueExist(String value)
	{
		return (attributes.containsValue(value));
	}
	
	public String getAttribute(String key)
	{
		return (attributes.get(key));
	}
	
	public void removeAttribute(String key)
	{
		attributes.remove(key);
	}
	
	public void clearAttributes()
	{
		attributes.clear();
	}
	
	public HashMap<String, String> getAttributesHashMap()
	{
		return attributes;
	}
	
	public void move(double x, double y)
	{
		this.setCoordinates(new Coordinates(this.getX() + x, this.getY() + y));
	}
	
	public void rotate(Point2D point, double rotateAngle)
	{
		rotateAngle = MathUtils.angleToPI(rotateAngle);
		
		double xp = Math.cos(rotateAngle) * (this.getX() - point.getX()) - Math.sin(rotateAngle) * (this.getY() - point.getY()) + point.getX();
		double yp = Math.cos(rotateAngle) * (this.getY() - point.getY()) + Math.sin(rotateAngle) * (this.getX() - point.getX()) + point.getY(); 
	
		this.setCoordinates(new Coordinates(xp, yp));
	}
	
	public void rotate(Point2D point, double rotateAngle, int rounded)
	{
		rotateAngle = MathUtils.angleToPI(rotateAngle);
		
		double xp = Math.cos(rotateAngle) * (this.getX() - point.getX()) - Math.sin(rotateAngle) * (this.getY() - point.getY()) + point.getX();
		double yp = Math.cos(rotateAngle) * (this.getY() - point.getY()) + Math.sin(rotateAngle) * (this.getX() - point.getX()) + point.getY(); 
	
		this.setCoordinates(new Coordinates(MathUtils.round(xp, rounded), MathUtils.round(yp, rounded)));
	}
	
	public void rotateWithPi(Point2D point, double rotateAngle)
	{	
		double xp = Math.cos(rotateAngle) * (this.getX() - point.getX()) - Math.sin(rotateAngle) * (this.getY() - point.getY()) + point.getX();
		double yp = Math.cos(rotateAngle) * (this.getY() - point.getY()) + Math.sin(rotateAngle) * (this.getX() - point.getX()) + point.getY(); 
	
		this.setCoordinates(new Coordinates(xp, yp));
	}
	
	public void rotateWithPi(Point2D point, double rotateAngle, int rounded)
	{	
		rotateAngle = MathUtils.angleToPI(rotateAngle);
		
		double xp = Math.cos(rotateAngle) * (this.getX() - point.getX()) - Math.sin(rotateAngle) * (this.getY() - point.getY()) + point.getX();
		double yp = Math.cos(rotateAngle) * (this.getY() - point.getY()) + Math.sin(rotateAngle) * (this.getX() - point.getX()) + point.getY(); 
	
		this.setCoordinates(new Coordinates(MathUtils.round(xp, rounded), MathUtils.round(yp, rounded)));
	}
	
	public void rotateSegmentFromStart(double rotateAngle)
	{
		rotateAngle = MathUtils.angleToPI(rotateAngle);
		
		double xp = Math.cos(rotateAngle) * this.getX() - Math.sin(rotateAngle) * this.getY();
		double yp = Math.cos(rotateAngle) * this.getY() + Math.sin(rotateAngle) * this.getX(); 
	
		this.setCoordinates(new Coordinates(xp, yp));
	}
	
	public void rotateSegmentFromStart(double rotateAngle, int rounded)
	{
		double xp = Math.cos(rotateAngle) * this.getX() - Math.sin(rotateAngle) * this.getY();
		double yp = Math.cos(rotateAngle) * this.getY() + Math.sin(rotateAngle) * this.getX(); 
	
		this.setCoordinates(new Coordinates(MathUtils.round(xp, rounded), MathUtils.round(yp, rounded)));
	}
	
	public void rotateSegmentFromStartWithPi(double rotateAngle)
	{
		double xp = Math.cos(rotateAngle) * this.getX() - Math.sin(rotateAngle) * this.getY();
		double yp = Math.cos(rotateAngle) * this.getY() + Math.sin(rotateAngle) * this.getX(); 
	
		this.setCoordinates(new Coordinates(xp, yp));
	}
	
	public void rotateSegmentFromStartWithPi(double rotateAngle, int rounded)
	{
		double xp = Math.cos(rotateAngle) * this.getX() - Math.sin(rotateAngle) * this.getY();
		double yp = Math.cos(rotateAngle) * this.getY() + Math.sin(rotateAngle) * this.getX(); 
	
		this.setCoordinates(new Coordinates(MathUtils.round(xp, rounded), MathUtils.round(yp, rounded)));
	}
	
	public void mirror(Axes2D axis)
	{
		if(axis.equals(Axes2D.AXIS_X))
		{
			this.setCoordinates(new Coordinates(this.getX(), -1 * this.getY() ));
		}
		
		if(axis.equals(Axes2D.AXIS_Y))
		{
			this.setCoordinates(new Coordinates(-1 * this.getX(), this.getY() ));
		}
		
		else
		{
			ConsoleLog.error(this.getClass(), "Undefined axis");
		}
	}

}
