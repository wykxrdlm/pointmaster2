package main.chebegroup.study.utils.geometry.geodesy;

public enum Axes2D 
{
	AXIS_X("AXES2D_AXIS_X"), 
	AXIS_Y("AXES2D_AXIS_Y");

	private final String code;

	private Axes2D(String code) 
	{
		this.code = code;
	}

	@Override
	public String toString() 
	{ 
		return String.valueOf(code);
	}
}