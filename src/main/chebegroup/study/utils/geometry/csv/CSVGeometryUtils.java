package main.chebegroup.study.utils.geometry.csv;

import java.util.ArrayList;

import main.chebegroup.study.utils.console.ConsoleLog;
import main.chebegroup.study.utils.csv.CSVLine;
import main.chebegroup.study.utils.csv.CSVTable;
import main.chebegroup.study.utils.geometry.Broken2D;
import main.chebegroup.study.utils.geometry.Point2D;
import main.chebegroup.study.utils.geometry.Segment2D;

public class CSVGeometryUtils 
{
	public static Point2D getPoint2DFromCSVTable(CSVTable table)
	{
		boolean flag = table.isOneLength();
		boolean flag1 = (table.maxLineLength() == 3 && table.minLineLength() == 3);
		
		boolean flag2 = table.lines() == 1;
		
		if (flag && flag1 && flag2)
		{
			String x = table.getElement(0, 1);
			String y = table.getElement(0, 2);
			
			if(isDouble(x) && isDouble(y) && table.getElement(0, 0).toUpperCase().equals(CSVPointDimensions.DIMENSION_2D.toString()))
			{
			
				return (new Point2D(Double.parseDouble(x), Double.parseDouble(y)));
			}
			else
			{
				ConsoleLog.error(CSVGeometryUtils.class, "Coordinates is not double or it is not 2D dimension");
				return null;
			}
			
		}
		else
		{
			ConsoleLog.error(CSVGeometryUtils.class, "This CSV is cannot parsed as Point2D");
			return null;
		}
	}
	
	public static Segment2D getSegment2DFromCSVTable(CSVTable table)
	{
		boolean flag = table.isOneLength();
		boolean flag1 = (table.maxLineLength() == 3 && table.minLineLength() == 3);
		
		boolean flag2 = table.lines() == 2;
		
		if (flag && flag1 && flag2)
		{
			String x = table.getElement(0, 1);
			String y = table.getElement(0, 2);
			
			String x1 = table.getElement(1, 1);
			String y1 = table.getElement(1, 2);
			
			if(isDouble(x) && isDouble(y) 
					&& table.getElement(1, 0).toUpperCase().equals(CSVPointDimensions.DIMENSION_2D.toString())
					&& table.getElement(0, 0).toUpperCase().equals(CSVPointDimensions.DIMENSION_2D.toString()))
			{
			
				return (new Segment2D(
								new Point2D(Double.parseDouble(x), Double.parseDouble(y)),
								new Point2D(Double.parseDouble(x1), Double.parseDouble(y1)))
						);
			}
			else
			{
				ConsoleLog.error(CSVGeometryUtils.class, "Coordinates is not double or it is not 2D dimension");
				return null;
			}
			
		}
		else
		{
			ConsoleLog.error(CSVGeometryUtils.class, "This CSV is cannot parsed as Segment2D");
			return null;
		}
	}
	
	public static Broken2D getBroken2DFromCSVTable(CSVTable table)
	{
		ArrayList<Point2D> points = new ArrayList<Point2D>();
		
		boolean flag = table.isOneLength();
		boolean flag1 = (table.maxLineLength() == 3 && table.minLineLength() == 3);
		boolean flag2 = (table.lines() > 1);
		
		if (flag && flag1 && flag2)
		{
			
			
			for (int i = 0; i < table.lines(); i++)
			{
			
				String x = table.getElement(i, 1);
				String y = table.getElement(i, 2);
				
				if(isDouble(x) && isDouble(y) && table.getElement(i, 0).toUpperCase().equals( CSVPointDimensions.DIMENSION_2D.toString()))
				{
				
					points.add(new Point2D(Double.parseDouble(x), Double.parseDouble(y)));
				}
				else
				{
					ConsoleLog.error(CSVGeometryUtils.class, "Coordinates is not double or it is not 2D dimension");
					return null;
				}
			
			}
			
			return (new Broken2D(points));
			
		}
		else
		{
			ConsoleLog.error(CSVGeometryUtils.class, "This CSV is cannot parsed as Broken2D");
			return null;
		}
	}
	
	public static ArrayList<Point2D> getPoint2DListFromCSVTable(CSVTable table)
	{
		ArrayList<Point2D> points = new ArrayList<Point2D>();
		
		boolean flag = table.isOneLength();
		boolean flag1 = (table.maxLineLength() == 3 && table.minLineLength() == 3);
		boolean flag2 = (table.lines() > 0);
		
		if (flag && flag1 && flag2)
		{
			
			
			for (int i = 0; i < table.lines(); i++)
			{
				String x = table.getElement(i, 1);
				String y = table.getElement(i, 2);
				
				if(isDouble(x) && isDouble(y) && table.getElement(i, 0).toUpperCase().equals( CSVPointDimensions.DIMENSION_2D.toString()))
				{
				
					points.add(new Point2D(Double.parseDouble(x), Double.parseDouble(y)));
				}
				else
				{
					ConsoleLog.error(CSVGeometryUtils.class, "Coordinates is not double or it is not 2D dimension");
					return null;
				}
			}
			
			return (points);
			
		}
		else
		{
			ConsoleLog.error(CSVGeometryUtils.class, "This CSV is cannot parsed as ArrayList of Point2D");
			return null;
		}
	}
	
	public static CSVTable getCSVTableFromPoint2D(Point2D point)
	{
		ArrayList<CSVLine> table = new ArrayList<CSVLine>();
		ArrayList<String> line = new ArrayList<String>();
		
		line.add(CSVPointDimensions.DIMENSION_2D.toString());
		line.add(String.valueOf(point.getX()));
		line.add(String.valueOf(point.getY()));
		
		CSVLine line_table = new CSVLine(line);
		table.add(line_table);
		
		return (new CSVTable(table));
	}
	
	public static CSVTable getCSVTableFromSegment2D(Segment2D segment)
	{
		ArrayList<CSVLine> table = new ArrayList<CSVLine>();
		ArrayList<String> line = new ArrayList<String>();
		ArrayList<String> line1 = new ArrayList<String>();
		
		Point2D point = segment.getFirstPoint();
		Point2D point1 = segment.getSecondPoint();
		
		line.add(CSVPointDimensions.DIMENSION_2D.toString());
		line.add(String.valueOf(point.getX()));
		line.add(String.valueOf(point.getY()));
		
		line1.add(CSVPointDimensions.DIMENSION_2D.toString());
		line1.add(String.valueOf(point1.getX()));
		line1.add(String.valueOf(point1.getY()));
		
		CSVLine line_table = new CSVLine(line);
		CSVLine line_table1 = new CSVLine(line);
		
		table.add(line_table);
		table.add(line_table1);
		
		return (new CSVTable(table));
	}
	
	public static CSVTable getCSVTableFromBroken2D(Broken2D broken)
	{
		ArrayList<CSVLine> table = new ArrayList<CSVLine>();
		
		ArrayList<Point2D> point_list = broken.getAllPoints();
		
		for (int j =0; j < point_list.size(); j++)
		{
			ArrayList<String> line = new ArrayList<String>();
			
			Point2D point = point_list.get(j);
			
			line.add(CSVPointDimensions.DIMENSION_2D.toString());
			line.add(String.valueOf(point.getX()));
			line.add(String.valueOf(point.getY()));
			
			CSVLine line_table = new CSVLine(line);
			table.add(line_table);
		}
		
		return (new CSVTable(table));
	}
	
	public static CSVTable getCSVTableFromPoint2DList(ArrayList<Point2D> points)
	{
		ArrayList<CSVLine> table = new ArrayList<CSVLine>();
		
		for (int j =0; j < points.size(); j++)
		{
			ArrayList<String> line = new ArrayList<String>();
			
			Point2D point = points.get(j);
			
			line.add(CSVPointDimensions.DIMENSION_2D.toString());
			line.add(String.valueOf(point.getX()));
			line.add(String.valueOf(point.getY()));
			
			CSVLine line_table = new CSVLine(line);
			table.add(line_table);
		}
		
		return (new CSVTable(table));
	}
	
	private static boolean isDouble(String str)
	{
		try
		{
			Double.parseDouble(str);
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}
}
