package main.chebegroup.study.utils.geometry.csv;

public enum CSVPointDimensions
{
	DIMENSION_2D("2D"), 
	DIMENSION_3D("3D");

	private final String code;

	private CSVPointDimensions(String code) 
	{
		this.code = code;
	}

	@Override
	public String toString() 
	{ 
		return String.valueOf(code);
	}
}
