package main.chebegroup.study.utils.geometry;

import java.util.ArrayList;

import main.chebegroup.study.utils.console.ConsoleLog;

public class Segment2D 
{
	Point2D first_point;
	Point2D second_point;
	
	public Segment2D(Point2D p1, Point2D p2)
	{
		if(!p1.getCoordinates().equals(p2.getCoordinates()))
		{
			this.first_point = p1;
			this.second_point = p2;
		}
		else
		{
			ConsoleLog.error(this.getClass(), "Incorrect points in the constructor of this class");
		}
	}
	
	public Point2D getFirstPoint()
	{
		return this.first_point;
	}
	
	public Point2D getSecondPoint()
	{
		return this.second_point;
	}
	
	public ArrayList<Point2D> getPoints()
	{
		ArrayList<Point2D> list = new ArrayList<Point2D>();
		list.add(first_point);
		list.add(second_point);
		
		return list;
	}
	
	public double length()
	{
		return first_point.getDist(second_point);
	}
	
	public boolean isPointInSegment(Point2D point)
	{
		double px = point.getX();
		double py = point.getY();
		
		double x1 = this.first_point.getX();
		double x2 = this.second_point.getX();
		
		double y1 = this.first_point.getY();
		double y2 = this.second_point.getY();
		
		double minx = Math.min(x1, x2);
		double maxx = Math.max(x1, x2);
		
		double miny = Math.min(y1, y2);
		double maxy = Math.max(y1, y2);
		
		double res = (px - x1)/(x2 - x1) - (py - y1)/(y2 - y1);
		
		boolean flag = (Math.abs(res) < MathConstants.e );
		boolean flag1 = (px >= minx && px <= maxx);
		boolean flag2 = (py >= miny && py <= maxy);
		
		if (flag && flag1 && flag2)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	public boolean isParallel(Segment2D segment)
	{
		double mx1 = this.first_point.getX();
		double mx2 = this.second_point.getX();
		
		double my1 = this.first_point.getY();
		double my2 = this.second_point.getY();
		
		double sx1 = segment.getFirstPoint().getX();
		double sx2 = segment.getSecondPoint().getX();
		
		double sy1 = segment.getFirstPoint().getY();
		double sy2 = segment.getSecondPoint().getY();
		
		double slope1 = (my1 - my2)/(mx1 - mx2);
		double slope2 = (sy1 - sy2)/(sx1 - sx2);
		
		return (slope1 == slope2);
	}
	
	public boolean isSegmentsCross(Segment2D segment)
	{
		
		//MAGIC CODE!
		//I DON'T KNOW HOW THIS WORKS
		//wykxrdlm
		
		double x1 = this.first_point.getX();
		double x2 = this.second_point.getX();
		
		double y1 = this.first_point.getY();
		double y2 = this.second_point.getY();
		
		double x3 = segment.getFirstPoint().getX();
		double x4 = segment.getSecondPoint().getX();
		
		double y3 = segment.getFirstPoint().getY();
		double y4 = segment.getSecondPoint().getY();
		
		double Ua, Ub, numerator_a, numerator_b, denominator;
		
		denominator=(y4-y3)*(x1-x2)-(x4-x3)*(y1-y2);
		
        if (denominator == 0)
        {
        	boolean flag = (x1*y2-x2*y1)*(x4-x3) - (x3*y4-x4*y3)*(x2-x1) == 0;
        	boolean flag1 = (x1*y2-x2*y1)*(y4-y3) - (x3*y4-x4*y3)*(y2-y1) == 0;
        	
            if (flag && flag1)  
            	return true;
            else 
            	return false;
        }
        else
        {
        	numerator_a=(x4-x2)*(y4-y3)-(x4-x3)*(y4-y2);
        	numerator_b=(x1-x2)*(y4-y2)-(x4-x2)*(y1-y2);
        	
        	Ua=numerator_a/denominator;
        	Ub=numerator_b/denominator;

        	if (Ua >=0 && Ua <=1 && Ub >=0 && Ub <=1)   
        		return true;
        	else    
        		return false;
        }
        
        
	}
}
