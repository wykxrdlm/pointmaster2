package main.chebegroup.study.utils.geometry;

import java.util.ArrayList;

import main.chebegroup.study.utils.console.ConsoleLog;

public class Point2D 
{
	private double x;
	private double y;
	
	public Point2D(double coordX, double coordY)
	{
		this.x = coordX;
		this.y = coordY;
	}
	
	public Point2D(Coordinates coordinates)
	{
		if(coordinates.is2D())
		{
			this.x = coordinates.getCoordinate(0);
			this.y = coordinates.getCoordinate(1);
		}
		else
		{
			ConsoleLog.error(this.getClass(), "Incorrect number of dimensions in constructor of this class");
		}
	}
	
	public void setCoordinates(Coordinates coordinates)
	{
		if(coordinates.is2D())
		{
			this.x = coordinates.getCoordinate(0);
			this.y = coordinates.getCoordinate(1);
		}
		else
		{
			ConsoleLog.error(this.getClass(), "Incorrect number of dimensions in setter of this class");
		}
	}
	
	public double getX()
	{
		return x;
	}
	
	public double getY()
	{
		return y;
	}
	
	public Coordinates getCoordinates()
	{
		ArrayList<Double> list = new ArrayList<Double>();
		list.add(this.x);
		list.add(this.y);
		
		Coordinates coords = new Coordinates(list);
		
		return coords;
	}
	
	public double getDist(Point2D another_point)
	{
		double x1 = this.x;
		double x2 = another_point.getX();
		
		double y1 = this.y;
		double y2 = another_point.getY();
		
		return Math.sqrt((y1-y2)*(y1-y2)+(x1-x2)*(x1-x2));
	}
}
