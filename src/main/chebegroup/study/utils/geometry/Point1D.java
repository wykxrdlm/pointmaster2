package main.chebegroup.study.utils.geometry;

import java.util.ArrayList;

import main.chebegroup.study.utils.console.ConsoleLog;

public class Point1D 
{
	private double x;
	
	public Point1D(double coordX)
	{
		this.x = coordX;
	}
	
	public Point1D(Coordinates coordinates)
	{
		if(coordinates.is1D())
		{
			this.x = coordinates.getCoordinate(0);
		}
		else
		{
			ConsoleLog.error(this.getClass(), "Incorrect number of dimensions in constructor of this class");
		}
	}
	
	public double getX()
	{
		return x;
	}
	
	public void setCoordinates(double x)
	{
		this.x = x;
	}
	
	public void setCoordinates(Coordinates coords)
	{
		if(coords.is1D())
		{
			this.x = coords.getCoordinate(0);
		}
		else
		{
			ConsoleLog.error(this.getClass(), "Incorrect number of dimensions in setter of this class");
		}
	}
	
	public Coordinates getCoordinates()
	{
		ArrayList<Double> list = new ArrayList<Double>();
		list.add(this.x);
		
		Coordinates coords = new Coordinates(list);
		
		return coords;
	}
	
	public double getDist(Point1D another_point)
	{
		return Math.abs(Math.max(this.getX(), another_point.getX())-Math.min(this.getX(), another_point.getX()));
	}
}
