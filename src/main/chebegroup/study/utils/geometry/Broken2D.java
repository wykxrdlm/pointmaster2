package main.chebegroup.study.utils.geometry;

import java.util.ArrayList;

import main.chebegroup.study.utils.console.ConsoleLog;

public class Broken2D 
{
	ArrayList<Point2D> points;
	ArrayList<Segment2D> segments;
	
	public Broken2D (ArrayList<Point2D> list)
	{
		this.points = list;
		ArrayList<Point2D> chlist = new ArrayList<Point2D>();
		
		for (int i = 0; i < list.size(); i++)
		{
			if(chlist.contains(list.get(i)))
			{
				ConsoleLog.error(getClass(), "Identical points!");
			}
			else
			{
				chlist.add(list.get(i));
			}
		}
		
		this.pointsToSegments();
	}
	
	public Broken2D (Point2D ... point2ds)
	{
		ArrayList<Point2D> list = new ArrayList<Point2D>();
		
		for (int i = 0; i < point2ds.length; i++)
		{
			if(list.contains(point2ds[i]))
			{
				ConsoleLog.error(getClass(), "Identical points!");
			}
			else
			{
				list.add(point2ds[i]);
			}
		}
		
		this.points = list;
		this.pointsToSegments();
	}
	
	private void pointsToSegments()
	{
		ArrayList<Segment2D> list = new ArrayList<Segment2D>();
		
		for (int i = 1; i < points.size(); i++)
		{
			list.add( new Segment2D(points.get(i-1), points.get(i)) );
		}
		
		this.segments = list;
		
	}
	
	public ArrayList<Point2D> getAllPoints()
	{
		return this.points;
	}
	
	public ArrayList<Segment2D> getAllSegments()
	{
		return this.segments;
	}
	
	public double length()
	{
		double length = 0;
		
		for (int i = 0; i < segments.size(); i++)
		{
			length = length + segments.get(i).length();
		}
		
		return length;
	}
	
	public boolean isPointInBroken(Point2D point)
	{
		for (int i = 0; i < segments.size(); i++)
		{
			if(segments.get(i).isPointInSegment(point))
				return true;
		}
		
		return false;
	}
	
	public boolean isSegmentCross(Segment2D segment)
	{
		for (int i = 0; i < segments.size(); i++)
		{
			if(segments.get(i).isSegmentsCross(segment))
				return true;
		}
		
		return false;
	}
	
	public boolean isBrokenCross(Broken2D segment)
	{
		ArrayList<Segment2D> list = segment.getAllSegments();
		
		for (int i = 0; i < segments.size(); i++)
		{
			for (int j = 0; j < list.size(); j++)
			{
				if(segments.get(i).isSegmentsCross(list.get(j)))
				{
					return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean isSelfCross()
	{		
		for (int i = 0; i < segments.size(); i++)
		{
			for (int j = 0; j < segments.size(); j++)
			{
				if(segments.get(i).isSegmentsCross(segments.get(j)))
				{
					return true;
				}
			}
		}
		
		return false;
	}
}
