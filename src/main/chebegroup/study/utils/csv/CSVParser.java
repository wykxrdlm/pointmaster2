package main.chebegroup.study.utils.csv;

import java.util.ArrayList;

public class CSVParser 
{
	public static ArrayList<String> parseCSVLineAsArrayList(String line)
	{
		String separator = ",";
		
		String[] parts = line.split(separator);
		
		ArrayList<String> list = new ArrayList<String>();
		for(int i = 0; i < parts.length; i++)
		{
			list.add(parts[i].toString());
		}
		
		return toNormalize(list);
	}
	
	public static ArrayList<String> parseCSVLineAsArrayList(String line, String separator)
	{
		
		String[] parts = line.split(separator);
		
		ArrayList<String> list = new ArrayList<String>();
		for(int i = 0; i < parts.length; i++)
		{
			list.add(parts[i].toString());
		}
		
		return toNormalize(list);
	}
	
	public static String[] parseCSVLineAsSimpleArray(String line)
	{
		String separator = ",";
		
		String[] parts = line.split(separator);
		
		return toNormalize(parts);
	}
	
	public static String[] parseCSVLineAsSimpleArray(String line, String separator)
	{
		
		String[] parts = line.split(separator);
		
		
		
		return toNormalize(parts);
	}
	
	public static ArrayList<ArrayList<String>> parseCSVAsArrayList(String lines)
	{
		String[] linesAll = lines.split("\n");
		ArrayList<ArrayList<String>> endList = new ArrayList<ArrayList<String>>();
		
		for (int i = 0; i < linesAll.length; i++)
		{
			if(!linesAll[i].isEmpty())
			{
				ArrayList<String> list = parseCSVLineAsArrayList(linesAll[i]);
				endList.add(list);
			}
		}
		
		return endList;
	}
	
	public static ArrayList<ArrayList<String>> parseCSVAsArrayList(String lines, String separator)
	{
		String[] linesAll = lines.split("\n");
		ArrayList<ArrayList<String>> endList = new ArrayList<ArrayList<String>>();
		
		for (int i = 0; i < linesAll.length; i++)
		{
			if(!linesAll[i].isEmpty())
			{
				ArrayList<String> list = parseCSVLineAsArrayList(linesAll[i], separator);
				endList.add(list);
			}
		}
			
		
		return endList;
	}
	
	public static String[][] parseCSVAsSimpleArray(String lines)
	{
		String[] linesAll = lines.split("\n");
		String[][] endList = new String[linesAll.length][];
		
		for (int i = 0; i < linesAll.length; i++)
		{
			if(!linesAll[i].isEmpty())
			{
				String[] arr = parseCSVLineAsSimpleArray(linesAll[i]);
				endList[i] = arr;
			}
		}
		
		return endList;
	}
	
	public static String[][] parseCSVAsSimpleArray(String lines, String separator)
	{
		String[] linesAll = lines.split("\n");
		String[][] endList = new String[linesAll.length][];
		
		for (int i = 0; i < linesAll.length; i++)
		{
			if(!linesAll[i].isEmpty())
			{
				String[] arr = parseCSVLineAsSimpleArray(linesAll[i], separator);
				endList[i] = arr;
			}
		}
		
		return endList;
	}
	
	private static String toNormalize(String line)
	{
		return line.trim();
	}
	
	private static String[] toNormalize(String[] lines)
	{
		String[] nl = new String[lines.length];
		
		for (int i = 0; i < nl.length; i++)
		{
			nl[i] = toNormalize(lines[i]);
		}
		
		return nl;
	}
	
	private static ArrayList<String> toNormalize(ArrayList<String> lines)
	{
		ArrayList<String> nl = new ArrayList<String>();
		
		for (int i = 0; i < lines.size(); i++)
		{
			nl.add(toNormalize(lines.get(i)));
		}
		
		return nl;
	}
}
