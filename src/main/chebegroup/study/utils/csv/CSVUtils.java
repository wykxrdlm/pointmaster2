package main.chebegroup.study.utils.csv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import main.chebegroup.study.utils.console.ConsoleLog;

public class CSVUtils 
{
	public static ArrayList<ArrayList<String>> openCSVFileAsArrayList(String path)
	{
		try
		{

			BufferedReader reader = new BufferedReader(new FileReader(path));
			String lines = "";
			String currentLine = "";
			while (currentLine != null)
			{
				lines = lines + currentLine + "\n";
				currentLine = reader.readLine();
			}
			
			reader.close();

			return CSVParser.parseCSVAsArrayList(lines);
		}
		catch(Exception e)
		{
			ConsoleLog.error(CSVUtils.class, "Exception while loading file");
			return null;
		}
	}
	
	public static CSVTable openCSVFileAsTable(String path)
	{
		try
		{

			BufferedReader reader = new BufferedReader(new FileReader(path));
			String lines = "";
			String currentLine = "";
			while (currentLine != null)
			{
				lines = lines + currentLine + "\n";
				currentLine = reader.readLine();
			}
			
			reader.close();

			return (new CSVTable(CSVParser.parseCSVAsArrayList(lines), null));
		}
		catch(Exception e)
		{
			ConsoleLog.error(CSVUtils.class, "Exception while loading file");
			return null;
		}
	}
	
	public static String[][] openCSVFileAsSimpleArray(String path)
	{
		try
		{

			BufferedReader reader = new BufferedReader(new FileReader(path));
			String lines = "";
			String currentLine = "";
			while (currentLine != null)
			{
				lines = lines + currentLine + "\n";
				currentLine = reader.readLine();
			}
			
			reader.close();

			return CSVParser.parseCSVAsSimpleArray(lines);
		}
		catch(Exception e)
		{
			ConsoleLog.error(CSVUtils.class, "Exception while loading file");
			return null;
		}
	}
	
	public static void writeCSVTable(String path, CSVTable table)
	{
		String separator = ",";
		
		try (FileWriter writer = new FileWriter(path, false))
		{
			ArrayList<ArrayList<String>> list = table.getList();
			
			String text = "";
			
			for (int i = 0; i < list.size(); i++)
			{
				String line = "";
				for(int j = 0; j < list.get(i).size(); j++)
				{
					line = line + list.get(i).get(j) + separator;
				}
				
				line = line.substring(0, line.length() - 1);
				text = text + line + "\n";
					
			}
			
			writer.write(text);

		}
		catch (Exception e)
		{
			ConsoleLog.error(CSVUtils.class, "Exception while saving file");
		}
	}
}
