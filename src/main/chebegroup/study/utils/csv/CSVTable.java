package main.chebegroup.study.utils.csv;

import java.util.ArrayList;

public class CSVTable 
{
	private ArrayList<CSVLine> table;
	
	public CSVTable(ArrayList<ArrayList<String>> list, String nulls)
	{
		ArrayList<CSVLine> newTable = new ArrayList<CSVLine>();
		
		for(int i = 0; i < list.size(); i++)
		{
			newTable.add(new CSVLine(list.get(i)));
		}
		
		this.table = newTable;
	}
	
	public CSVTable(ArrayList<CSVLine> list)
	{
		this.table = list;
	}
	
	public int maxLineLength()
	{
		int maxLength = 0;
		for (int i = 0; i < table.size(); i++)
		{
			maxLength = Math.max(table.get(i).length(), maxLength);
		}
		
		return maxLength;
	}
	
	public int minLineLength()
	{
		int minLength = Integer.MAX_VALUE;
		for (int i = 0; i < table.size(); i++)
		{
			minLength = Math.min(table.get(i).length(), minLength);
		}
		
		return minLength;
	}
	
	public boolean isOneLength()
	{
		if(table.size() > 0)
		{
			int normalLength = table.get(0).length();
			
			for (int i = 0; i < table.size(); i++)
			{
				if(table.get(i).length() != normalLength) return false;
			}
		
		}
		
		return true;
	}
	
	public String getElement(int i, int j)
	{
		return table.get(i).getElement(j);
	}
	
	public CSVTable toOneSize(int size)
	{
		ArrayList<CSVLine> newTable = new ArrayList<CSVLine>();
		for(int i = 0; i < table.size(); i++)
		{
			ArrayList<String> l = new ArrayList<String>();
			
			int k = 0;
			for(int j = 0; j < table.get(i).length() && j < size; j++)
			{
				l.add(table.get(i).getElement(j));
				k = j;
			}
			
			if (size > table.get(i).length())
			{
				for(int k1 = k; k1 < size; k1++)
				{
					l.add("");
				}
			}
			
		}
		
		return (new CSVTable(newTable));
	}
	
	public ArrayList<ArrayList<String>> getList()
	{
		ArrayList<ArrayList<String>> ls = new ArrayList<ArrayList<String>>();
		
		for (int i =0; i < table.size(); i++)
		{
			ls.add(table.get(i).getList());
		}
		
		return ls;
	}
	
	public CSVTable toOneSize(int size, int size2)
	{
		ArrayList<CSVLine> newTable = new ArrayList<CSVLine>();
		for(int i = 0; i < table.size() && i < size2; i++)
		{
			ArrayList<String> l = new ArrayList<String>();
			
			int k = 0;
			for(int j = 0; j < table.get(i).length() && j < size; j++)
			{
				l.add(table.get(i).getElement(j));
				k = j;
			}
			
			if (size > table.get(i).length())
			{
				for(int k1 = k; k1 < size; k1++)
				{
					l.add("");
				}
			}
			
		}
		
		return (new CSVTable(newTable));
	}
	
	public int lines()
	{
		return table.size();
	}
}
