package main.chebegroup.study.utils.csv;

import java.util.ArrayList;

import main.chebegroup.study.utils.console.ConsoleLog;

public class CSVLine 
{
	private ArrayList<String> tableLine;
	
	public CSVLine(ArrayList<String> line)
	{
		this.tableLine = line;
	}
	
	public int length()
	{
		return tableLine.size();
	}
	
	public String getElement(int i)
	{
		if (i > length()-1 || i < 0)
		{
			ConsoleLog.error(getClass(), "Incorrect index");
			return null;
		}
		else
		{
			return tableLine.get(i);
		}
		
	}
	
	public ArrayList<String> getList()
	{
		return tableLine;
	}
}
