package main.chebegroup.study.utils.console;

public class ConsoleColoredOutput 
{
	public static void outLine(String message)
	{
		System.out.println(message);
	}
	
	public static void outLine(ColorTerminal color, String message)
	{
		System.out.println(color.toString() + message + ColorTerminal.RESET);
	}
	
	public static void outLine(ColorTerminal color, ColorTerminalBackground bg, String message)
	{
		System.out.println(color.toString() + bg.toString() + message + ColorTerminal.RESET);
	}
}
