package main.chebegroup.study.utils.console;

public enum ColorTerminalBackground 
{
	RESET("\033[0m"),
	
	BLACK_BACKGROUND("\033[40m"),
	RED_BACKGROUND("\033[41m"),
	GREEN_BACKGROUND("\033[42m"),
	YELLOW_BACKGROUND("\033[43m"),
	BLUE_BACKGROUND("\033[44m"),
	MAGENTA_BACKGROUND("\033[45m"),
	CYAN_BACKGROUND("\033[46m"),
	WHITE_BACKGROUND("\033[47m"),
	
	BLACK_BACKGROUND_BRIGHT("\033[0;100m"),
	RED_BACKGROUND_BRIGHT("\033[0;101m"),
	GREEN_BACKGROUND_BRIGHT("\033[0;102m"),
	YELLOW_BACKGROUND_BRIGHT("\033[0;103m"),
	BLUE_BACKGROUND_BRIGHT("\033[0;104m"),
	MAGENTA_BACKGROUND_BRIGHT("\033[0;105m"),
	CYAN_BACKGROUND_BRIGHT("\033[0;106m"),
	WHITE_BACKGROUND_BRIGHT("\033[0;107m");
	
	private final String code;

	ColorTerminalBackground(String code) 
	{
		this.code = code;
	}

	@Override
	public String toString() 
	{
		return code;
	}
}
