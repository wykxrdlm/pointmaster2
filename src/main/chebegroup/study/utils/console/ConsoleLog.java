package main.chebegroup.study.utils.console;

public class ConsoleLog 
{
	public static void log(Class<?> obj, String msg)
	{
		System.out.println("[" + obj.getName() + "] LOG: " + msg);
	}
	
	public static void error(Class<?> obj, String msg)
	{
		System.out.println("[" + obj.getName() + "] ERROR: " + msg);
		System.exit(0);
	}
	
	public static void separator()
	{
		int num = 64;
		String sep = "-";
		
		String line = "";
		
		for(int i = 0; i < num; i++)
		{
			line = line + sep;
		}
		
		System.out.println(line);
	}
	
	public static void separator(int num)
	{
		//int num = 64;
		String sep = "-";
		
		String line = "";
		
		for(int i = 0; i < num; i++)
		{
			line = line + sep;
		}
		
		System.out.println(line);
	}
	
	public static void separator(String sep)
	{
		int num = 64;
		//String sep = "-";
		
		String line = "";
		
		for(int i = 0; i < num; i++)
		{
			line = line + sep;
		}
		
		System.out.println(line);
	}
	
	public static void separator(int num, String sep)
	{
		//int num = 64;
		//String sep = "-";
		
		String line = "";
		
		for(int i = 0; i < num; i++)
		{
			line = line + sep;
		}
		
		System.out.println(line);
	}
}
