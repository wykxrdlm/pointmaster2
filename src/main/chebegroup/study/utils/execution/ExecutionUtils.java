package main.chebegroup.study.utils.execution;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ExecutionUtils 
{
	public static String getAbsolutePathToProgram()
	{
		try
		{
			URL res = ExecutionUtils.class.getClassLoader().getResource("");
			File file = Paths.get(res.toURI()).toFile();
			String absolutePath = file.getAbsolutePath();
			//System.out.println(absolutePath);
			return absolutePath;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return "";
		}
		
		
	}
	
	public static List<String> executeConsoleCommand(String command)
	{
		try
		{
			ProcessBuilder pb = new ProcessBuilder(command);
			
			Process p = pb.start();
			BufferedReader br=new BufferedReader(new InputStreamReader(p.getInputStream()));
			List<String> lineList = new ArrayList<>();
			String line;
			int max_count = 0;
			while(max_count <= 4) 
			{
				line = br.readLine();
				
				if (line == null || line.isEmpty())
					max_count += 1;
				else
					max_count = 0;
				
				if (line != null)
					lineList.add(line);
			}
			br.close();
			
			return lineList;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<String> executeConsoleCommand(String[] command)
	{
		try
		{
			ProcessBuilder pb = new ProcessBuilder(command);
	
			Process p = pb.start();
			BufferedReader br=new BufferedReader(new InputStreamReader(p.getInputStream()));
			List<String> lineList = new ArrayList<>();
			String line;
			int max_count = 0;
			while(max_count <= 4) 
			{
				line = br.readLine();
				
				if (line == null || line.isEmpty())
					max_count += 1;
				else
					max_count = 0;
				
				if (line != null)
					lineList.add(line);
			}
			br.close();
			
			return lineList;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static void executeConsoleCommandNoLog(String command)
	{
		try
		{
			Runtime.getRuntime().exec(command);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void executeConsoleCommandNoLog(String[] command)
	{
		try
		{
			Runtime.getRuntime().exec(command);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void printList(List<String> list, String outputMode)
	{
		outputMode = outputMode.toUpperCase();
		
		if (list != null)
		{
			//COUNTLINES
			if (outputMode.equals("COUNTLINES"))
			{
				for(int i = 0; i < list.size(); i++)
				{
					System.out.println("(" + String.valueOf(i+1) + "): " + list.get(i));
				}
			}
			//DASH
			else if (outputMode.equals("DASH"))
			{
				for(int i = 0; i < list.size(); i++)
				{
					System.out.println(" - " + list.get(i));
				}
			}
			//SIMPLE
			else
			{
				for(int i = 0; i < list.size(); i++)
				{
					System.out.println(list.get(i));
				}
			}
		}
		
	}
	
	public static void printList(List<String> list)
	{
		for(int i = 0; i < list.size(); i++)
		{
			System.out.println(list.get(i));
		}
	}
	
	public static String[] listToExecutionCommand(List<String> list)
	{
		return (String[]) list.toArray(new String[0]);
	}
	
	public static boolean checkFileName(String fileName)
	{
		String[] blocked = new String[] {"+", "|", ">", "<", "\"", "?", "*", ":", "/", "\\", " ", "%", "!", "@"};
		int dots = 0;
		for (int i = 0; i < fileName.length(); i++)
		{
			if(fileName.charAt(i) == '.')
			{
				dots+=1;
			}
		}
		
		if (dots>1) return false;
		
		for(int j = 0; j < blocked.length; j++)
		{
			if (fileName.contains(blocked[j]))
			{
				return false;
			}
		}
		
		return true;
	}
}
